from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import UserDetail
from rest_framework import status
from .serializers import UserSerializer


class UserDataApi(APIView):

    def get(self, request):
        # name = request.GET.get('name')
        param = request.GET.get('name')
        userdata = UserDetail.objects.all()
        # if param.keys() == 'email':
        # # email = request.GET.get('email')
        #     user_data = UserDetail.objects.get(email=param.get('email'))
        # if param.keys() is None:
        #     qs = UserDetail.objects.all()
        #     users = {}
        #     for data in qs:
        #         users[data.id] = {'user': data.name, 'email': data.email}
        #     return Response(
        #         users
        #     )
        # else:
        #     return Response({
        #         'data': None
        #     })
        # return Response({
        #     # 'user': user_data.name,
        #     # 'email': user_data.email,
        #     # 'address': user_data.address,
        #     'users': users
        # })
        serializer = UserSerializer(userdata, many=True)
        return Response(serializer.data)

    # def post(self, request):
    #     name = request.data.get('name')
    #     email = request.data.get('email')
    #     address = request.data.get('address')
    #     user = UserDetail.objects.create(name=name, email=email, address=address)
    #     return Response({'id':user.id,'name': user.name, 'email': user.email, 'address': user.address}, status=status.HTTP_201_CREATED)

    def post(self, request):
        user_data = request.data
        password = user_data.pop('password')
        serializer = UserSerializer(data=user_data)
        if serializer.is_valid():
            v = serializer.validated_data
            user = UserDetail.objects.create(**v, password=password)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
