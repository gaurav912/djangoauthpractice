from django.urls import path
from .views import UserDataApi
from .models import UserDetail
urlpatterns = [
    path('users/', UserDataApi.as_view(), name='user_detail')
]