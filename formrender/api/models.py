from django.db import models


# Create your models here.
class UserDetail(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    address = models.CharField(max_length=200)
    password = models.CharField(max_length=100, default='')
