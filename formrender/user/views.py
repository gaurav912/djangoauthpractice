from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Group
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from .forms import RegisterForm
from django import forms
from django.contrib.auth import authenticate, login as dj_login
from django.http import HttpResponse
from django.views.generic import View, ListView, DeleteView, DetailView, UpdateView, CreateView
from rest_framework.views import APIView
from rest_framework.response import Response

# # # @login_required
# def welcome(request):
#     username = User.objects.get(username=request.user)
#     return render(request, 'base.html', {'user': username})
#
#
# def register_form(request):
#     if request.method == 'POST':
#         form = RegisterForm(request.POST)
#         if form.is_valid():
#             first_name = form.cleaned_data['first_name']
#             last_name = form.cleaned_data['last_name']
#             username = form.cleaned_data['username']
#             email = form.cleaned_data['email']
#             password = form.cleaned_data['password']
#             form.save()
#         return redirect('logs')
#     else:
#         form = RegisterForm()
#
#     return render(request, 'register.html', {'form': form})
#
#
# def login_form(request):
#     if request.method == 'POST':
#         form = LoginForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data.get('username')
#             password = form.cleaned_data.get('password')
#             user = authenticate(username=username, password=password)
#             if user is not None:
#                 dj_login(request, user)
#                 return redirect('welcome')
#             else:
#                 return HttpResponse("Invalid Login attempt")
#     else:
#         form = LoginForm()
#     return render(request, 'login.html', {'login_form': form})
def welcome(request):
    template_name = 'welcome.html'
    return render(request, template_name)


class RegisterView(View):
    form_class = RegisterForm
    model = User
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save()
            password = form.cleaned_data.get('password')
            user.set_password(password)
            user.save()
            return redirect('login')
        else:
            return render(request, self.template_name, {'form': form})


# class LoginView(View):
#     form_class = LoginForm
#     model = User
#     template_name = 'login.html'
#
#     def get(self, request, *args, **kwargs):
#         form = self.form_class()
#         return render(request, self.template_name, {'form': form})
#
#     def post(self, request, *args, **kwargs):
#         form = self.form_class(request.POST)
#         username = form.data.get("username")
#         password = form.data.get("password")
#         user = authenticate(username=username, password=password)
#         if user:
#             dj_login(request, user)
#             return redirect('welcome')
#         else:
#             return render(request, self.template_name, {'form': form})
#
#


@method_decorator(login_required, name='dispatch')
class HomeView(View):
    template_name = 'home.html'

    def get(self, request):
        if request.user.is_authenticated:
            username = request.user.first_name
            return render(request, self.template_name, {'username': username})

#        implementing ListView gave restricted operation
# class ShowUser(ListView):
#     template_name = 'user_list.html'
#     queryset = User.objects.all()
#     content_object_name = 'users'
#     paginate_by = 5
#     ordering = ['username']


# class ShowGroup(ListView):
#     template_name = 'group_list.html'
#     queryset = Group.objects.all()
#     current_user =
#     context_object_name = 'groups'
#     paginate_by = 5
#     ordering = ['name']


@method_decorator(login_required, name='dispatch')
class ShowUser(View):
    template_name = 'user_list.html'

    def get(self, request):
        is_admin = False
        users = User.objects.all()
        current_user = User.objects.get(username=request.user)
        try:
            groups = current_user.groups.all()
            for user_group in groups:
                if user_group.id == Group.objects.get(name="admin").id:
                    is_admin = True
                    break
        except Exception as e:
            pass
        finally:
            return render(request, self.template_name, {'users': users, 'is_admin': is_admin})


@method_decorator(login_required, name='dispatch')
class ShowGroup(View):
    template_name = 'group_list.html'

    def get(self, request):
        is_admin = False
        groups = Group.objects.all()
        current_user = User.objects.get(username=request.user)
        try:
            user_groups = current_user.groups.all()
            for user_group in user_groups:
                if user_group.id == Group.objects.get(name="admin").id:
                    is_admin = True
                    break
        except Exception as e:
            pass
        finally:
            return render(request, self.template_name, {'groups': groups, 'is_admin': is_admin})


@method_decorator(login_required, name='dispatch')
class DeleteUser(DeleteView):
    model = User
    success_url = reverse_lazy('user_list')


@method_decorator(login_required, name='dispatch')
class DeleteGroup(DeleteView):
    model = Group
    success_url = reverse_lazy('group_list')


@method_decorator(login_required, name='dispatch')
class EditUser(UpdateView):
    model = User
    template_name_suffix = '_update_form'
    fields = ['first_name', 'last_name', 'username', 'email',
              'is_active', 'is_staff', 'is_superuser', 'groups', 'date_joined']
    success_url = reverse_lazy('user_list')


@method_decorator(login_required, name='dispatch')
class EditGroup(UpdateView):
    model = Group
    template_name_suffix = '_update_form'
    fields = ['name', 'permissions']
    success_url = reverse_lazy('group_list')


@method_decorator(login_required, name='dispatch')
class AddUser(CreateView):
    model = User
    template_name_suffix = '_create_form'
    fields = ['first_name', 'last_name', 'username', 'password', 'email',
              'is_active', 'is_staff', 'is_superuser', 'groups', 'date_joined']
    success_url = reverse_lazy('user_list')

    # overrided this method from CreateView to add widgets
    def get_form(self):
        form = super(AddUser, self).get_form()
        form.fields['password'].widget = forms.PasswordInput()
        return form

    # overrided this method from CreateView to manually set password in hashed form.
    def form_valid(self, form):
        user = form.save()
        user.set_password(form.cleaned_data.get('password'))
        user.save()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class AddGroup(CreateView):
    model = Group
    template_name_suffix = '_create_form'
    fields = ['name', 'permissions']
    success_url = reverse_lazy('group_list')


@method_decorator(login_required, name='dispatch')
class ViewUserDetails(DetailView):
    model = User


@method_decorator(login_required, name='dispatch')
class ViewGroupDetails(DetailView):
    model = Group




