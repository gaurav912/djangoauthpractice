from django import forms
from django.contrib.auth.models import User


class RegisterForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }
        help_texts = {
            # 'username': ''
        }

#                   this template is not in use if we use LoginView of auth_views
# class LoginForm(forms.ModelForm):
#     # username = forms.CharField(max_length=100, error_messages={'required': 'username should not be empty'})
#     password = forms.CharField(max_length=100, required=True, widget=forms.PasswordInput)
#
#     class Meta:
#         model = User
#         fields = ['username', 'password']
#         help_texts = {
#             'username': ''
#         }
#         error_messages = {
#             'invalid_login':
#                 "Please enter a correct %(username)s and password. Note that both "
#                 "fields may be case-sensitive."
#         }
