from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('signup', views.RegisterView.as_view(), name='register'),
    path('login', auth_views.LoginView.as_view(template_name="login.html"), name='login'),
    path('logout', auth_views.LogoutView.as_view(template_name="login.html"), name='logout'),
    path('home', views.HomeView.as_view(), name='home'),
    path('home/users', views.ShowUser.as_view(), name='user_list'),
    path('home/groups', views.ShowGroup.as_view(), name='group_list'),
    path('home/users/<int:pk>/delete', views.DeleteUser.as_view(), name='delete_user'),
    path('home/groups/<int:pk>/delete', views.DeleteGroup.as_view(), name='delete_group'),
    path('home/users/<int:pk>/edit', views.EditUser.as_view(), name='edit_user'),
    path('home/groups/<int:pk>/edit', views.EditGroup.as_view(), name='edit_group'),
    path('home/users/add_user', views.AddUser.as_view(), name='add_user'),
    path('home/groups/add_group', views.AddGroup.as_view(), name='add_group'),
    path('home/users/<int:pk>/details', views.ViewUserDetails.as_view(), name='view_user_details'),
    path('home/groups/<int:pk>/details', views.ViewGroupDetails.as_view(), name='view_group_details'),

]
